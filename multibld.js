/**
 * Generates random 3x3 Rubik's cube scramble with random orientation.
 * 
 * Only proxy to scramble script, which I downloaded from qqtimer.net timer.
 * The scramble script needs to be included before this file.
 */
function generateScramble() {
    return scrambler.getRandomScramble() + randomCubeOrientation();
}

/**
 * Cube class.
 * Cube objects are part of Attempt.
 * Random scramble is generated on creation.
 * The cube is implicitely solved, it can be
 * set to unsolved when the attempt is finished.
 */
class Cube {
    constructor() {
        this.scramble = generateScramble();
        this.solved = true;
    }
}

/**
 * Details of mistake of some cube.
 */
class Mistake {
    /**
     * Creates Mistake instance.
     * Possible values for mistake, phase and pieceType 
     * are declared in global `multiBlindValueLists` variable.
     * 
     * @param {Number} cubeIndex Index of the cube in Attempt context. 
     * @param {String} mistake Name of the mistake. 
     * @param {String} phase Phase the mistake occured in. Can be empty string.
     * @param {String} pieceType Piece type this mistake affected.
     */
    constructor(cubeIndex, mistake, phase, pieceType) {
        this.cubeIndex = cubeIndex;
        this.mistake = mistake;
        this.phase = phase;
        this.pieceType = pieceType;
    }
}

/**
 * Duration class is used to store the duration
 * of an attempt with second accuracy.
 * 
 * Each duration is normalised.
 * That means that 74 minutes will be 
 * automatically converted to 1 hour and 14 minutes.
 */
class Duration {
    constructor(hours, minutes, seconds) {
        this.hours = parseInt(hours);
        this.minutes = parseInt(minutes);
        this.seconds = parseInt(seconds);
        this.normalise();
    }

    /** 
     * Normalises this duration.
     * That means that e.g. 74 minutes will be 
     * automatically converted to 1 hour and 14 minutes.
     * */
    normalise() {
        let leftover = parseInt(this.seconds / 60);
        this.seconds %= 60;
        this.minutes += leftover;
        leftover = parseInt(this.minutes / 60);
        this.minutes %= 60;
        this.hours += leftover;
    }

    /**
     * Get length of this duration in seconds.
     * @returns Duration converted to seconds.
     */
    get totalSeconds() {
        return (this.hours * 60 + this.minutes) * 60 + this.seconds;
    }

    /** 
     * Get text representation of a Duration.
     * @returns HH:MM:SS string of this Duration 
     */
    toString() {
        let pad = t => new String(t).padStart(2, "0");
        return `${pad(this.hours)}:${pad(this.minutes)}:${pad(this.seconds)}`;
    }
}

/**
 * This class is used to simulate Rubik's cube
 * scrambling in a model. The model can be rendered later.
 */
class CubePreviewModel {
    constructor() {
        this.SVG_NS = "http://www.w3.org/2000/svg";
        this.ROWS = 3; // rows of one Rubik's cube side
        this.COLUMNS = 3; // columns of one Rubik's cube side

        this.resetCube(); // initialisation of the inner model
    }

    /** Reset cube to solved state. */
    resetCube() {
        /** Rubik's cube representation */
        this._model = {
            U: ["w", "w", "w",
                "w", "w", "w",
                "w", "w", "w"],
            F: ["g", "g", "g",
                "g", "g", "g",
                "g", "g", "g"],
            R: ["r", "r", "r",
                "r", "r", "r",
                "r", "r", "r"],
            L: ["o", "o", "o",
                "o", "o", "o",
                "o", "o", "o"],
            D: ["y", "y", "y",
                "y", "y", "y",
                "y", "y", "y"],
            B: ["b", "b", "b",
                "b", "b", "b",
                "b", "b", "b"]
        }
    }

    /** Util function for rendering */
    createSvgElem(name) {
        return document.createElementNS(this.SVG_NS, name);
    }

    /** Renders top (U) side of the inner cube model into a svgElem */
    renderTopSide(svgElem) {
        for (let i = 0; i < this.COLUMNS; i++) {
            for (let j = 0; j < this.ROWS; j++) {
                const sticker = this.createSvgElem("rect");
                sticker.setAttribute("x", i * 30);
                sticker.setAttribute("y", j * 30);
                sticker.setAttribute("width", 30);
                sticker.setAttribute("height", 30);
                sticker.setAttribute("fill", this.getStickerColor("U", j, i));
                sticker.setAttribute("stroke", "black");

                svgElem.appendChild(sticker);
            }
        }
    }

    /**
     * Resets cube model and then applies a scramble to it.
     * 
     * @param {String} scramble standard notation scramble,
     * uppercase normal moves, lowercase rotations.
     * */
    scramble(scramble) {
        this.resetCube();
        var buffer = "";
        var isMoveChar = c => ['R', 'U', 'F', 'B', 'L', 'D', 'x', 'y', 'z'].includes(c);
        var isControlChar = c => ['\'', '2'].includes(c);

        for (var c of scramble) {
            if (isMoveChar(c)) {
                if (buffer.length > 0) {
                    this.scrambleMove(buffer);
                }

                buffer = c;
            } else if (isControlChar(c)) {
                if (buffer.length == 1) {
                    buffer += c;
                    this.scrambleMove(buffer);
                    buffer = "";
                }
            }
        }

        if (buffer.length > 0) {
            this.scrambleMove(buffer);
        }
    }

    /** Util function used to scramble one move */
    scrambleMove(move) {
        switch (move) {
            case "R": return this._R();
            case "U": return this._U();
            case "F": return this._F();
            case "B": return this._B();
            case "L": return this._L();
            case "D": return this._D();
            case "x": return this._x();
            case "y": return this._y();
            case "z": return this._z();
            case "R'": return this._invertMove(() => this._R());
            case "U'": return this._invertMove(() => this._U());
            case "F'": return this._invertMove(() => this._F());
            case "B'": return this._invertMove(() => this._B());
            case "L'": return this._invertMove(() => this._L());
            case "D'": return this._invertMove(() => this._D());
            case "x'": return this._invertMove(() => this._x());
            case "y'": return this._invertMove(() => this._y());
            case "z'": return this._invertMove(() => this._z());
            case "R2": return this._doubleMove(() => this._R());
            case "U2": return this._doubleMove(() => this._U());
            case "F2": return this._doubleMove(() => this._F());
            case "B2": return this._doubleMove(() => this._B());
            case "L2": return this._doubleMove(() => this._L());
            case "D2": return this._doubleMove(() => this._D());
            case "x2": return this._doubleMove(() => this._x());
            case "y2": return this._doubleMove(() => this._y());
            case "z2": return this._doubleMove(() => this._z());
        }
    }

    /** 
     * Gets color name of some face (e.g. R), row (0-2) and column (0-2) 
     * @returns color name, e.g. "yellow" or "red"
     */
    getStickerColor(face, row, column) {
        const index = row * this.ROWS + column;
        return this.getColorName(this._model[face][index]);
    }

    getColorName(abbr) {
        const colorNames = {
            "w": "white",
            "g": "green",
            "r": "red",
            "b": "blue",
            "o": "orange",
            "y": "yellow"
        }

        return colorNames[abbr];
    }
    
    _clockwiseRotateFaceOnly(faceAbbr) {
        let f = this._model[faceAbbr];

        let rotated = [
            f[6], f[3], f[0],
            f[7], f[4], f[1],
            f[8], f[5], f[2]
        ];

        this._model[faceAbbr] = rotated;
    }

    /** 
     * Rotate only face, not whole side. 
     * Impossible move on real cube, only utility for generating other moves.
     * @param faceAbbr uppercase face abbreviation, e.g. 'U'
     * @param times how many times the move has to be performed, can be also negative for opposite direction moves
     */
    rotateFaceOnly(faceAbbr, times) {
        this._perform(() => this._clockwiseRotateFaceOnly(faceAbbr), times);
    }

    _perform(func, times) {
        for (let i = 0; i < ((times % 4) + 4) % 4; i++) { // complicated modulo 4, because negative 'times' is allowed
            func();
        }
    }

    /** 
     * Performs inverted move 
     * @param func function of the move
     */
    _invertMove(func) {
        this._perform(func, -1);
    }

    /** 
     * Performs move twice 
     * @param func function of the move
     */
    _doubleMove(func) {
        this._perform(func, 2);
    }

    /* BASE MOVES */
    _x() {
        const buffer = this._model.F;
        this._model.F = this._model.D;
        this._model.D = this._model.B;
        this.rotateFaceOnly("D", 2);
        this._model.B = this._model.U;
        this.rotateFaceOnly("B", 2);
        this._model.U = buffer;
        this.rotateFaceOnly("L", -1);
        this.rotateFaceOnly("R", 1);
    }

    _y() {
        const buffer = this._model.F;
        this._model.F = this._model.R;
        this._model.R = this._model.B;
        this._model.B = this._model.L;
        this._model.L = buffer;
        this.rotateFaceOnly("U", 1);
        this.rotateFaceOnly("D", -1);
    }

    _R() {
        const indices = [2, 5, 8];

        for (const i of indices) {
            this.rotateFaceOnly("B", 2); // orientation fix for B
            let buffer = this._model.U[i];
            this._model.U[i] = this._model.F[i];
            this._model.F[i] = this._model.D[i];
            this._model.D[i] = this._model.B[i];
            this._model.B[i] = buffer;
            this.rotateFaceOnly("B", 2);
        }

        this.rotateFaceOnly("R", 1);
    }

    /* DERIVED MOVES */

    _conjugate(a, b) {
        a();
        b();
        this._invertMove(a);
    }

    /**
     * Derived moves will be generated using conjugate patern
     * especially: "Rotation -> Right side move -> Rotation back"
     */
    _conjugateR(move) {
        this._conjugate(move, () => this._R());
    }

    _z() {
        this._conjugate(() => this._x(), () => this._y());
    }

    _U() {
        this._conjugateR(() => this._z())
    }

    _F() {
        this._conjugateR(() => this._invertMove(() => this._y()))
    }

    _B() {
        this._conjugateR(() => this._y());
    }

    _L() {
        this._conjugateR(() => this._doubleMove(() => this._y()));
    }

    _D() {
        this._conjugateR(() => this._invertMove(() => this._z()));
    }


}

/**
 * This class represents MultiBLD attempt.
 */
class Attempt {
    constructor(attempted) {
        this.cubes = [];
        this.attempted = attempted;
        this.inProgress = true;
        this.startTimestamp = Date.now();
        this.duration = new Duration(0, 59, 59);
    }

    /** 
     * Sets number of attempted cubes,
     * creates/removes cubes (with scrambles) if necessary
     */
    set attempted(attempted) {
        for (let i = this.attempted; i < attempted; i++) {
            this.cubes.push(new Cube());
        }

        for (let i = this.attempted; i > attempted; i--) {
            this.cubes.pop();
        }

        this.solved = this.attempted;
    }

    /** Returns number of attempted cubes. */
    get attempted() {
        return this.cubes.length;
    }

    /**
     * Sets duration of this attempt.
     * Also sets this attempt's timestamp
     * to now.
     * 
     * @param {Duration} value new duration of the attempt
     */
    set duration(value) {
        this._duration = value;
        this.timestamp = Date.now();
    }

    /** Gets duration of this attempt. */
    get duration() {
        return this._duration;
    }

    /** 
     * Sets number of solved cubes after an attempt.
     * @param {Number} count
     */
    set solved(count) {
        if (!this.validateIndex(count)) {
            return;
        }

        this._solved = count;
    }

    /**
     * Gets number of solved cubes. 
     * If not yet set, it's as all attempted cubes are solved
     */
    get solved() {
        return this._solved;
    }

    /**
     * Sets number of solved cubes.
     * @param wrongCubes array of the indices of cubes to be set to unsolved
     */
    set wrong(wrongCubes) {
        let wrongCubeIndexes = wrongCubes.split(',')
            .map(i => parseInt(i.trim()) - 1)
            .filter(i => !isNaN(i)).sort();

        if (!this.validateIndexes(wrongCubeIndexes)) {
            console.log("Incorrect indexes");
            return;
        }

        this._wrongCubeIndexes = wrongCubeIndexes;

        for (let cube of this.cubes) {
            cube.solved = true;
        }

        for (const i of this._wrongCubeIndexes) {
            this.cubes[i].solved = false;
        }
    }

    /** Gets array of wrong, unsolved cubes (Cube object array) */
    get wrong() {
        return this.cubes.filter(cube => !cube.solved);
    }

    /** Gets array of indices of wrong cubes */
    get wrongIndexes() {
        return (this._wrongCubeIndexes === undefined)
            ? []
            : this._wrongCubeIndexes;
    }

    /** Gets number of cubes, which have been specified unsolved */
    get knownWrongCount() {
        return this.wrong.length;
    }

    /** Gets total number of unsolved cubes, even these which were not identified. */
    get totalWrongCount() {
        return this.attempted - this.solved;
    }

    /** Returns last wrong cube, Cube object. */
    get lastWrong() {
        return this.wrong[this.wrong.length - 1];
    }

    /** Returns cube index of last wrong cube. */
    get lastWrongIndex() {
        return this.wrongIndexes[this.wrongIndexes.length - 1];
    }

    /** Returns points of this attempt, special formula, WCA standard. */
    get points() {
        return this.solved - this.totalWrongCount;
    }

    /** Returns time per solved cubes, Duration object. */
    get timePerSolved() {
        return new Duration(0, 0, this.duration.totalSeconds / this.solved);
    }

    validateIndex(index) {
        return index <= this.attempted && index >= 0;
    }

    validateIndexes(indexes) {
        if (indexes.length > this.attempted) {
            return false;
        }

        for (const index of indexes) {
            if (!this.validateIndex(index)) {
                console.log("Not valid", index)
                return false;
            }
        }

        return true;
    }

    offsetWrongIndex(cubeIndex, offset) {
        const offsetIndex = this._wrongCubeIndexes.indexOf(cubeIndex) + offset;
        return this._wrongCubeIndexes[offsetIndex];
    }

    getNextWrongIndex(cubeIndex) {
        return this.offsetWrongIndex(cubeIndex, 1);
    }

    getPreviousWrongIndex(cubeIndex) {
        return this.offsetWrongIndex(cubeIndex, -1);
    }

    /**
     * Gets string of this attempt,
     * including how many cubes solved,
     * how many cubes attempted and duration of this attempt.
     */
    toString() {
        return Attempt.getString(this);
    }

    static getString(attempt) {
        return `${attempt._solved}/${attempt.attempted} za ${attempt.duration.toString()}`;
    }

    /**
     * Entities from indexedDb have to be converted,
     * because Attempt class is not just data
     * - it has a lot of useful functions.
     * 
     * @param {Object} e Attempt entity from database. 
     */
    static fromEntity(e) {
        let ret = new Attempt(0);
        ret.cubes = e.cubes;
        ret._solved = e._solved;
        ret._wrongCubeIndexes = e._wrongCubeIndexes;
        ret.inProgress = e.inProgress;
        ret.timestamp = e.timestamp;
        ret.startTimestamp = e.startTimestamp;

        if (e._duration) {
            ret._duration = new Duration(e._duration.hours, e._duration.minutes, e._duration.seconds);
        }

        return ret;
    }

}

/**
 * Database for Attempt storage.
 * Uses indexedDb.
 * Uses callbacks.
 */
class MultiBlindDB {
    constructor() {
        let idb = this.open();

        idb.onupgradeneeded = function (e) {
            let db = e.target.result;
            db.createObjectStore("results", { autoIncrement: true });
        }
    }

    /**
     * Open indexedDb connection.
     * If schema is changed during developement,
     * increment DB_VERSION constant in code.
     */
    open() {
        const DB_NAME = "muzatmatMultiBlindDB";
        const DB_VERSION = 1;
        return indexedDB.open(DB_NAME, DB_VERSION);
    }

    /**
     * Create or update (if exists) attempt in database.
     * @param {Attempt} attempt
     */
    putResult(attempt) {
        this.open().onsuccess = function (e) {
            let db = e.target.result;
            let t = db.transaction("results", "readwrite");
            t.objectStore("results").put(attempt, attempt.startTimestamp);
        }
    }

    /** Gets all attempts from indexedDb and performs callback on them. */
    getAllResults(callback) {
        this.open().onsuccess = function (e) {
            let db = e.target.result;
            let t = db.transaction("results", "readonly");
            let r = t.objectStore("results").getAll();

            r.onsuccess = function (e) {
                callback(e.target.result);
            }
        };
    }

    /** Gets last attempt from indexedDb and performs callback with it. */
    getLast(callback) {
        this.open().onsuccess = function (e) {
            let db = e.target.result;
            let t = db.transaction("results", "readonly");
            let r = t.objectStore("results").openCursor(null, "prev");

            r.onsuccess = e => {
                if (e.target.result) {
                    callback(e.target.result.value);
                } else {
                    callback(null);
                }
            }
        }
    }

    /** Asks for confirmation, then deletes whole database. */
    delete() {
        confirm("Are you sure to delete ALL results?");
        let r = indexedDB.deleteDatabase("muzatmatMultiBlindDB");
    }
}

/**
 * Main application class.
 * 
 * Used only for the solving process.
 * It's used mainly for rendering logic,
 * also connects all of the above components to one system.
 */
class Application {
    constructor() {
        this._cubeRenderer = new CubePreviewModel();
        this._db = new MultiBlindDB();
        this.setupAttempt(() => {
            this._currentTemplate = "input-digit";
            this.render();
            window.onhashchange = () => this.render();
        });
    }

    /**
     * Creates new Attempt and assigns it to this Application,
     * or uses the last Attempt in the database if it's inProgress.
     */
    setupAttempt(callback) {
        this._db.getLast(last => {
            if (last != null && last.inProgress) {
                this._attempt = Attempt.fromEntity(last);
                this._attempt.duration = new Duration(0, 7, 32);
            } else {
                this._attempt = new Attempt(1);
                window.location.hash = "input-attempted";
            }

            callback();
        });
    }

    /**
     * Sets template based on name.
     * 
     * Uses templates with elements with empty elements.
     * Sets this template to main element
     * @param {String} value name of the new set template
     */
    set _currentTemplate(value) {
        let templateContainer = document.querySelector("main");

        switch (value) {
            case "input-digit":
                templateContainer.innerHTML = `
                    <label for="how-many-input"></label> </br>
                    <input type="number" id="how-many-input" />
                `;
                break;
            case "input-text":
                templateContainer.innerHTML = `
                    <label for="text-input"></label> </br>
                    <input type="text" id="text-input" />
                `;
                break;
            case "scramble":
                templateContainer.innerHTML = `
                    <h2></h2>
                    <span id="scramble"></span>
                    <svg id="cube-preview" viewBox="0 0 90 90">
                    </svg>
                `;
                break;
            case "repair":
                templateContainer.innerHTML = `
                    <h2></h2>
                    <span id="scramble"></span>
                    <form id="mistake-form">
                        <label for="mistake">Chyba:</label>
                        <select id="mistake"/></select> <br/>
                        <label for="phase">Fáze:</label>
                        <select id="phase"> </select><br/>
                        <label for="piece-type">Typ dílku</label>
                        <select id="piece-type"></select>
                    </form>
                `;
                break;
            case "audio":
                templateContainer.innerHTML = `
                    <h2></h2>
                    <p></p>
                    <audio controls></audio>
                `;
                break;
            case "summary":
                templateContainer.innerHTML = `
                    <h2></h2>
                    <p id="summary"></p>
                `;
                break;
            case "results-table":
                templateContainer.innerHTML = `
                    <table id="results-table">
                        <thead>
                        <tr><th>Datum zápisu</th> <th>Výsledek</th></tr>
                        </thead>
                        
                        <tbody>
                        </tbody>
                    </table>
                `;
                break;
            case "input-duration":
                templateContainer.innerHTML = `
                    <h2></h2>
                    <form id="duration">
                        <input id="hours" tabindex="1" type="number" /> 
                        <span class="semicolon-big">:</span> 
                        <input id="minutes" tabindex="2" type="number" />
                        <span class="semicolon-big">:</span> 
                        <input id="seconds" tabindex="3" type="number" />
                    </form>
                `;
                break;
            default:
                templateContainer.innerHTML = "";
        }
    }

    /** 
     * Sets "previous arrow" link href.
     * 
     * @param {String} href relative address of previous arrow
     */
    set _previous(href) {
        document.getElementById("prev").setAttribute("href", href);
    }

    /** 
     * Sets "next arrow" link href.
     * 
     * @param {String} href relative address of next arrow
     */
    set _next(href) {
        document.getElementById("next").setAttribute("href", href);
    }


    /**
     * Renders page by current url's hash.
     * On default, renders "Input attempted cubes" screen.
     */
    render() {
        let args = window.location.hash.split('_');

        switch (args[0].slice(1)) {
            case "input-attempted":
                this.renderInputAttempted();
                break;
            case "cube":
                this.renderScramble(parseInt(args[1]) - 1);
                break;
            case "repair-cube":
                this.renderRepairCube(parseInt(args[1]) - 1);
                break;
            case "input-solved":
                this.renderInputSolved();
                this._previous = "#input-duration";
                this._next = "#input-which-wrong";
                break;
            case "input-which-wrong":
                this.renderInputWhichWrong();
                break;
            case "add-errors":
                this.renderErrorAdding();
                break;
            case "solving":
                this.renderSolving();
                break;
            case "summary":
                this.renderSummary();
                break;
            case "all-results":
                this._attempt.inProgress = false;
                this.updateDB();
                this.renderAllResults();
                break;
            case "input-duration":
                this.renderInputDuration();
                break;
            default:
                this.renderMainScreen();

        }
    }

    /* SCREEN RENDERING FUNCTIONS */

    renderNumberInputScreen(label) {
        this._currentTemplate = "input-digit";
        document.querySelector("main > label").textContent = label;
    }

    renderTextInputScreen(label) {
        this._currentTemplate = "input-text";
        document.querySelector("main > label").textContent = label;
    }

    renderInputAttempted() {
        this.renderNumberInputScreen("Kolik kostek budeš skládat?");
        const input = document.querySelector("main input");
        input.setAttribute("min", 1);
        input.setAttribute("value", this._attempt.attempted);
        let change = () => this._attempt.attempted = parseInt(input.value);
        input.addEventListener("change", change);

        this._next = "#cube_1";
        this._previous = "Index.html";

        /* Note probably only for submission */
        this.renderNote(`
            Uživatel si může zvolit počet
            kostek, které se později pokusí složit poslepu.
            Podle tohoto počtu budou posléze vygenerována
            rozložení. Ihned po zvolení počtu lze pokračovat dále.
        `);
    }

    renderInputSolved() {
        this.renderNumberInputScreen("Kolik kostek jsi složil?");
        const input = document.querySelector("main input");
        input.setAttribute("min", 0);
        input.setAttribute("value", this._attempt.solved);
        input.setAttribute("max", this._attempt.attempted);
        let change = () => this._attempt.solved = parseInt(input.value);
        input.addEventListener("change", change);

        /* Note probably only for submission */
        this.renderNote(`
            Zde může uživatel zadat,
            kolik kostek se mu podařilo složit.
            Počet kostek je shora omezen počtem zkoušených kostek.    
        `)
    }

    renderScramble(cubeIndex) {
        if (cubeIndex == 0) {
            this.updateDB();
        }

        this._currentTemplate = "scramble";

        const scramble = this._attempt.cubes[cubeIndex].scramble;

        document.querySelector("main h2")
            .textContent = `Kostka ${cubeIndex + 1}`;
        document.getElementById("scramble")
            .textContent = scramble;
        

        this._cubeRenderer.scramble(scramble);
        console.log(this._cubeRenderer._model)
        this._cubeRenderer.renderTopSide(document.getElementById("cube-preview"));

        this._next = (cubeIndex + 1 == this._attempt.attempted)
            ? "#solving"
            : `#cube_${cubeIndex + 2}`;

        this._previous = (cubeIndex == 0)
            ? "#input-attempted"
            : `#cube_${cubeIndex}`;

        /* Note probably only for submission */
        this.renderNote(`
            Rozložení je vygenerováno skriptem z timeru qqtimer.net.
            Je zapsáno pomocí značení, které se využívá i na soutěžích.
            Apostrof značí rotaci dané stěny proti směru hodinových ručiček.
            Na obrázku je vidět horní stěna daného rozložení (svg formát).
        `)
    }

    renderRepairCube(cubeIndex) {
        this._currentTemplate = "repair";
        this._next = (cubeIndex == this._attempt.lastWrongIndex)
            ? "#summary"
            : `#repair-cube_${this._attempt.getNextWrongIndex(cubeIndex) + 1}`;
        this._previous = (cubeIndex == this._attempt.wrongIndexes[0])
            ? "#input-which-wrong"
            : `#repair-cube_${this._attempt.getPreviousWrongIndex(cubeIndex) + 1}`;

        document.querySelector("main h2")
            .textContent = `Oprava kostky ${cubeIndex + 1}`;
        document.getElementById("scramble")
            .textContent = this._attempt.cubes[cubeIndex].scramble;

        const phaseInput = document.querySelector("#phase");
        const mistakeInput = document.querySelector("#mistake");
        const pieceTypeInput = document.querySelector("#piece-type");

        for (const value of multiBlindValueLists.phases) {
            let option = document.createElement("option");
            option.textContent = value;
            phaseInput.add(option);
        }

        for (const value of multiBlindValueLists.mistakes) {
            let option = document.createElement("option");
            option.textContent = value;
            mistakeInput.add(option);

        }

        for (const value of multiBlindValueLists.pieces) {
            let option = document.createElement("option");
            option.textContent = value;
            pieceTypeInput.add(option);
        }

        let change = () => {
            const desc = mistakeInput.value;
            const phase = phaseInput.value;
            const pieceType = pieceTypeInput.value;

            let mistake = new Mistake(cubeIndex, desc, phase, pieceType);
            this._attempt.cubes[cubeIndex].mistake = mistake;
        }

        mistakeInput.addEventListener("change", change);
        phaseInput.addEventListener("change", change);
        pieceTypeInput.addEventListener("change", change);

        /* Note probably only for submission */
        this.renderNote(`
            Uživatel může opět zamíchat kostku,
            která se mu nepovedla, a zjistit, v čem udělal chybu.
            Chybu pak upřesní pomocí selectů.
            Chyba se později uloží do databáze,
            v grafu bude možné vidět, v čem uživatel celkově chybuje nejvíce.
        `)
    }

    renderMainScreen() {
        this.renderInputAttempted();
    }

    renderSolving() {
        this._db.putResult(this._attempt);

        this._previous = `#cube_${this._attempt.attempted}`;
        this._next = "#input-duration";

        this._currentTemplate = "audio";
        document.querySelector("main h2")
            .textContent = "Čas na skládání";
        document.querySelector("main p")
            .innerHTML = `
            Nyní máš čas na skládání. <br>
            Můžeš si k tomu pustit zvuky přírody 😆 <br>
            Počítač ale můžeš i vypnout, rozložení zůstanou uložená v prohlížeči. <br>
            Tato aplikace neměří čas, ten můžeš změřit např. na hodinkách, stopkách, telefonu.
        `;

        const audio = document.querySelector("main audio");
        audio.setAttribute("src", "calming.mp3");
    }

    renderInputWhichWrong() {
        this.renderTextInputScreen("Které kostky byly špatně? (Pořadí, kostky odděleny čárkou)");

        const input = document.querySelector("main input");
        input.setAttribute("value", this._attempt.wrongIndexes.map(i => i + 1).join(','));
        let setWrong = () => this._attempt.wrong = input.value;
        let setNext = () => {
            this._next = (this._attempt.knownWrongCount > 0)
                ? `#repair-cube_${this._attempt.wrongIndexes[0] + 1}`
                : "#summary";
        }
        let change = () => { setWrong(); setNext(); this.updateDB() };
        input.addEventListener("change", change);

        this._previous = "#input-solved";
        setNext();

        /* Note probably only for submission */
        this.renderNote(`
            Pokud uživatel ví, které kostky byly špatně,
            vyplní jejich pořadí, oddělena čárkou
            (tedy např. pokud chyboval v první a čtvrté, vyplní '1,4').
            Po pokračování mu budou opět předloženy
            jejich rozložení, aby mohl zjistit příčinu chyby.
        `)
    }

    renderSummary() {
        this._currentTemplate = "summary";
        document.querySelector("main h2")
            .textContent = "Potvrzení výsledku"
        document.getElementById("summary")
            .innerText = `Pokračováním bude pokus
                "${this._attempt.toString()}"
                dokončen a zapsán do databáze v prohlížeči.
            `;

        this._previous = (this._attempt.knownWrongCount > 0)
            ? `#repair-cube_${this._attempt.lastWrongIndex + 1}`
            : "#input-solved";

        this._next = "#all-results";

        /* Note probably only for submission */
        this.renderNote("Pokračováním bude aktuální pokus uzavřen a uložen do databáze.")
    }

    renderAllResults() {
        this._currentTemplate = "results-table";
        this._previous = "#summary";
        this._next = "Index.html";

        this._db.getAllResults(results => {
            const tbody = document.querySelector("#results-table tbody");
            for (let result of results) {
                console.log(result);
                result = Attempt.fromEntity(result);
                let tr = document.createElement("tr");
                tr.innerHTML = `<td>${new Date(result.timestamp).toDateString()}</td><td>${Attempt.getString(result)}</td>`;
                tbody.append(tr);
            }
        });
    }

    renderInputDuration() {
        this._currentTemplate = "input-duration";
        this._previous = "#solving"
        this._next = "#input-solved";

        document.querySelector("main h2").textContent = "Napiš celkový čas trvání pokusu (HH:MM:SS)";
        let hoursInput = document.getElementById("hours");
        let minutesInput = document.getElementById("minutes");
        let secondsInput = document.getElementById("seconds");

        hoursInput.setAttribute("min", 0);
        minutesInput.setAttribute("min", 0);
        secondsInput.setAttribute("min", 0);
        minutesInput.setAttribute("max", 60);
        secondsInput.setAttribute("max", 60);

        let set = (h, m, s) => this._attempt.duration = new Duration(h, m, s);
        let setAll = () => set(parseInt(hoursInput.value), parseInt(minutesInput.value), parseInt(secondsInput.value));
        let change = () => { setAll(); this.updateDB() }

        hoursInput.addEventListener("change", change);
        minutesInput.addEventListener("change", change);
        secondsInput.addEventListener("change", change);

        hoursInput.setAttribute("value", new String(this._attempt.duration.hours).padStart(2, "0"));
        minutesInput.setAttribute("value", new String(this._attempt.duration.minutes).padStart(2, "0"));
        secondsInput.setAttribute("value", new String(this._attempt.duration.seconds).padStart(2, "0"));

        /* Note probably only for submission */
        this.renderNote(`
            Na této obrazovce uživatel zadá
            dobu trvání pokusu, kterou změřil např. na hodinkách, nebo stopkách
            (tato aplikace čas neměří).
        `)
    }

    /**
     * Appends note node to main element.
     * 
     * These notes will be probably only used for school
     * submission, so the application behaviour is more clear.
     * 
     * @param {String} note text of the node to render (description etc.)
     */
    renderNote(note) {
        var paragraph = document.createElement("p");
        paragraph.className = "submission-note";
        paragraph.textContent = note;
        document.querySelector("main").appendChild(paragraph);
    }

    /** 
     * Updates current attempt to database.
     * Creates it if not exists (distinguished by creation timestamp).
     */
    updateDB() {
        this._db.putResult(this._attempt);
    }
}

/* CHART FUNCTIONS */

/**
 * Parent class for all charts. Has result DB inside.
 */
class MultiBlindChart {
    constructor() {
        this._db = new MultiBlindDB();
    }
}

/**
 * Line chart of "Time per solved cube".
 */
class MultiBlindTimePerSolvedChart extends MultiBlindChart {
    /**
     * Creates new chart and appends it to context element.
     * @param {*} context canvas's context element
     */
    constructor(context) {
        super();
        this._db.getAllResults(results => {
            results = results.map(e => Attempt.fromEntity(e));
            var labels = results
                .map(result => new Date(result.timestamp))
                .map(date => date.toDateString());
            var data = results
                .map(result => result.timePerSolved)
                .map(duration => duration.totalSeconds)
            this.chart = new Chart(context, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Čas na jednu složenou kostku',
                        data: data,
                        backgroundColor: "rgba(0, 0, 0, 0)",
                        borderColor: [
                            'rgba(64, 64, 255, 1)'
                        ],
                        borderWidth: 2,
                        lineTension: 0
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value, index, values) {
                                    return new Duration(0, 0, value).toString();
                                }
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                return new Duration(0, 0, parseInt(tooltipItem.value)).toString();
                            }
                        }
                    }
                }
            });
        });
    }
}

/**
 * Errors distribution pie chart.
*/
class MultiBlindErrorsPieChart extends MultiBlindChart {
    /**
     * Creates new chart and appends it to context element.
     * @param {*} context canvas's context element
     */
    constructor(context) {
        super();
        this._db.getAllResults(results => {
            results = results.map(e => Attempt.fromEntity(e));
            var mistakes = results
                .map(r => r.wrong)
                .reduce((acc, curr) => acc.concat(curr), [])
                .filter(cube => cube.mistake)
                .map(cube => cube.mistake.mistake);
            var counts = [], labels = [];
            for (const mistake of mistakes) {
                if (labels.includes(mistake)) {
                    counts[labels.indexOf(mistake)]++;
                } else {
                    labels.push(mistake);
                    counts.push(1);
                }
            }

            const STEP = (256 / labels.length) * 3;
            let colors = [];

            for (let r = 0; r < 256; r += STEP) 
                for (let g = 0; g < 256; g += STEP) 
                    for (let b = 0; b < 256; b += STEP)
                        colors.push(`rgb(${r},${g},${b})`);
            
            new Chart(context, {
                type: 'pie',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Poměr chyb',
                        data: counts,
                        backgroundColor: colors
                    }]
                }
            });
        });
    }
}


/* DATA */

/** Allowed values when repairing errors */
var multiBlindValueLists = {
    "phases": ["", "pamatování", "vzpomínání", "skládání"],
    "pieces": ["", "hrany", "rohy"],
    "mistakes": [
        "podobně vypadající cykly",
        "přehmat - wide tah",
        "twist/flip",
        "špatný setup",
        "zapomněl vrátit rotaci",
        "špatná exec - algy s Rw, Lw",
        "špatný směr",
        "nevzpomněl na první pár",
        "mystery chyba",
        "vynechal pár",
        "správný dílek špatná nálepka",
        "zapomenutý pár uprostřed",
        "špatný pár",
        "twist/flip vynechal",
        "nový alg chybka",
        "flip/twist + parity",
        "vracení setupu",
        "jiný dílek místo bufferu",
        "krkolomnost",
        "parity - vynechal",
        "nedodělal poslední tah",
        "vracení se",
        "pár hran uprostřed rohů"]
}