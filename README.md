# MultiBLD
## Instalace
```
git clone git@gitlab.fel.cvut.cz:muzatmat/multibld.git
```

Soubor `calming.mp3` nahradíme uklidňujícím zvukem
dle vlastního výběru.
Vstupní brána do aplikace je soubor `Index.html`.

## Použití
Na hlavní stránce si vybereme "Nový pokus",
pokud chceme zapsat nový výsledek. 
Otevře se průvodce. Za jeho průběhu je pokus
průběžně automaticky ukládán do databáze v prohlížeči.
Data v ní zůstávají i po vypnutí prohlížeče / počítače.

Kvůli komplikovanému procesu / odvětví jsem doplnil poznámky
k důležitým obrazovkám.

Po dokončení pokusu je uživatel navrácen na hlavní obrazovku.
Tam si může zobrazit statistiky o zlepšování a poměru chyb.

## Splněné bonusové požadavky a kde je najít
Kód se nachází v následujících souborech:
- Hlavní stránka
    - Index.html
    - Styles_main-screen.css
- Průvodce (Nový pokus)
    - multibld.html
    - multibld.js - většina kódu, třídy
    - Styles.css
- Statistiky
    - stats.html
    - Styles_stats.css
    - multibld.js (načtení dat z DB a generování grafů)

### Grafika - SVG
V hlavním menu v `Index.html` jsou SVG vektorové
ikonky. Ty jsem stáhnul z internetu, a jejich text zkopíroval
přímo do dokumentu.

Udělal jsem to tímto způsobem proto, aby byla celá struktura
součástí html dokumentu a já mohl ikonky stylovat pomocí CSS.
Po najetí na ikonku se ikonce změní barva z černé na bílou.

Tato SVG lze najít v souboru `Index.html`, barvení
ve `Styles_main-screen.css` u selektorů `a svg *`,
`a:hover svg path` a `a:hover svg rect`.

### Média - Audio
V průvodci (Nový pokus, `multibld.html`) obrazovce "Čas na skládání" (`/multibld.html#solving`)
je možné si pustit zvuk `calming.mp3` z kořenové složky.

Tato obrazovka je generována funkcí `Application.prototype.renderSolving`.

### Formulářové prvky
V průvodci (Nový pokus, `multibld.html`).
Např pro zadávání počtu zkoušených kostek,
hned na začátku při vytváření nového pokusu.

U zadávání číselných hodnot jsou nastaveny
horní a dolní meze z logiky věci (počet složených nemůže být více než počet zkoušených).
Nastavení takové meze pomocí atributů `min` a `max`
lze vidět např v ve funkci `Application.prototype.renderInputSolved()`, soubor `multibld.js`.

### Offline aplikace
Aplikace je koncipována jako offline aplikace.
Uchovávání dat (předchozích pokusů) funguje pomocí indexedDb.
O to se stará třída `MultiBlindDB`, soubor `multibld.js`.

### Media queries
Pokud se obrazovka zúží pod 800 px, průvodce se přizpůsobí.

Šipky jsou v html hned za sebou, v css se pak mění jejich
pořadí, podle toho, jestli je šířka okna nižší než 800px.

Také se mění velikost písem (rozložení, apod.).

Media queries lze nalézt v souboru `Styles.css`.

### Použití JS frameworku či knihovny
Pro generování grafů jsem použil Chart.js.
Ta generuje grafy do kontextu canvasu.

Původně jsem chtěl použít Flot, ale zjistil jsem,
že k němu je potřeba jQuery, a to jsem do tohoto
projektu nechtěl zanášet. Chtěl jsem to udělat co nejjednodušší.

Generování statistik je vidět ve třídách `MultiBlindTimePerSolvedChart`
a `MultiBlindErrorsPieChart`, soubor `multibld.js`.
`Chart.js` musí být ve stránce nebo v bundlu dříve.

### Funkční historie
Přechod mezi jednotlivými obrazovkami v prohlížeči
se děje po kliknutí na kotvu `<a id="prev">`
nebo `<a id="next">` s atributem `href` rovnému
hashi následující či předchozí stránce.

Na window je navěšen posluchač na událost hashchange,
který překreslí stránku podle aktuálního hashe
ve funkci `Application.prototype.render()` v souboru `multibld.js`.

### JS práce se SVG
Při zobrazení rozložení kostek v úvodu průvodce `multibld.html`
se zobrazí náhled horní strany kostky.

Tento náhled (js model kostky a posléze vykreslení do svg elementu)
řeší třída `CubePreviewModel`, soubor `multibld.js`.

## Zadání (dříve odevzdáno)
Student vytvoří tréninkovou platformu na disciplínu MultiBLD.
Platforma bude realizovaná pomocí single page aplikace.
Aplikace bude obsahovat průvodce pro zápis dat a přehled dat s grafy.
Aplikace bude fungovat bez serveru (zjednodušení pro účely semestrální práce)

### Motivace
Ve svém volném čase se věnuji multiBLD - disciplína 
zapamatování a složení co největšího počtu Rubikových kostek poslepu. 

Na data o svém zlepšování používám Google Sheets. 
Tam mám jeden list s výsledky, každý výsledek má své ID.
Tohle ID pak používám v jiných listech, pro záznam podmínek tréninku, 
a chyb které jsem udělal - podobně jako u relačních databází.
Další list pak používám na seznamy povolených hodnot (podmínek, chyb).
V posledním listu mám grafy, kde můžu lépe sledovat svůj pokrok.

Tabulkový procesor je mocný nástroj na takový úkol,
ale myslím, že celý proces mého tréninku by se dal zjednodušit.
Např. zadávání časů není ideální, vždy musím krkolomně psát 0:YY:ZZ,
přestože můj čas je jen YY:ZZ.

V **KAJ** se dbá na to, aby semestrální projekt nebylo nic šuplíkového,
ale něco co bychom mohli reálně využívat. Plánuji toho tedy využít 
a vytvořit aplikaci, která pokryje celý proces - 
od generování rozložení až po správu *chyb* a zakreslení tréninkových dat do grafů.

### Způsob zpracování
V úvodní stránce aplikace uživatel dostane na výběr,
zdali chce trénovat či zobrazit přehled (před tréninkem nechci vidět data z minulosti).

#### Trénink
Tréninková část aplikace bude realizována formou průvodce.

Můj trénink nyní probíhá následovně:
- Míchání kostek
    - Na mobilu mám aplikaci FiveTimer, podle ní zamíchám první kostku
    - Pak zapnu a vypnu stopky v aplikaci, tím se vygeneruje další rozložení
    - Proces opakuji dokud nezamíchám všechny kostky
    - Rozložení jsou uložená, abych se k nim mohl později vrátit
- Samotný trénink
    - Čas měřím pomocí stopek na hodinkách (nechci zbytečně používat mobil či PC)
- Zápis výsledku
    - Do Google Sheets zapíšu počet složených kostek, počet zkoušených kostek a čas
- Sebereflexe
    - Pomocí dříve uložených rozložení zjišťuji, v čem jsem dělal chyby
    - Zapíšu do listu *chyby*, pomocí ID z listu *výsledky*
- Zápis podmínek
    - Do listu *podmínky* zapíšu všechny podmínky (denní doba, rušení, pocity)
- Kouknu se na grafy, jak se mi daří pokrok

Každá z výše zmíněných částí bude jednou čí více obrazovkami průvodce.
Průvodce se vždy zeptá na vše potřebné, vygeneruje a zobrazí rozložení,
případně předvyplní další obrazovky.
Mezi průvodcem bude možné se pohybovat pomocí šipek na klávesnici,
posunem prstu na dotykovém displeji nebo tlačítky na obrazovce.

Pro zjednodušení semestrální práce nechci používat server,
poslední obrazovka průvodce bude požadovat .json soubor s "databází"
výsledků uživatele, ten dovyplní tímto novým výsledkem a vrátí opět .json soubor "ke stažení", uživatel může přepsat starý.

Později po odevzdání semestrální práce, pokud mi bude aplikace užitečná,
přidělám si k ní jednoduchou serverovou aplikaci s databází pro ukládání výsledků.

#### Zobrazení přehledu
"Analytická" část aplikace bude obsahovat grafy o pokroku:
- Liniové grafy
    - *Body* (podle data, *Body = počet složených - počet nesložených*)
    - Čas na jednu kostku (podle data)
    - Čas na jednu složenou kostku (podle data)
- Koláčové grafy
    - Jaké chyby dělám nejčastěji
    - Jaké mám nejčastěji podmínky

Grafy realizuji pomocí některé javascriptové knihovny, nejspíše [Flot](https://www.flotcharts.org/flot/examples/).